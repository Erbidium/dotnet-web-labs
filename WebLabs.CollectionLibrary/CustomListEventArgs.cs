﻿namespace CollectionLibrary;

public class CustomListEventArgs<T> : EventArgs
{
    public CustomListEventArgs(T value)
    {
        Value = value;
    }

    public T Value { get; }
}
