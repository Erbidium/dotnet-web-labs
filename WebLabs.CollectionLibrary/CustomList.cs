using System.Collections;

namespace CollectionLibrary;

public class CustomList<T>: IList<T> where T: IEquatable<T>
{
    private const int DefaultCapacity = 4;
    private int _size;
    private T[] _data;

    public event EventHandler<CustomListEventArgs<T>>? ElementAdded;
    public event EventHandler<CustomListEventArgs<T>>? ElementRemoved;

    public CustomList()
    {
        _data = Array.Empty<T>();
    }

    public CustomList(int capacity)
    {
        if (capacity < 0)
        {
            throw new ArgumentOutOfRangeException(nameof(capacity));
        }
        _data = new T[capacity];
    }
    
    public CustomList(IEnumerable<T>? collection)
    {
        switch (collection)
        {
            case null:
                throw new ArgumentNullException(nameof(collection));
            case ICollection<T> c:
            {
                var count = c.Count;
                if (count == 0)
                {
                    _data = Array.Empty<T>();
                }
                else
                {
                    _data = new T[count];
                    c.CopyTo(_data, 0);
                    _size = count;
                }

                break;
            }
            default:
            {
                _data = Array.Empty<T>();
                using var en = collection.GetEnumerator();
                while (en.MoveNext())
                {
                    Add(en.Current);
                }

                break;
            }
        }
    }

    public IEnumerator<T> GetEnumerator()
    {
        for (var i = 0; i < _size; i++)
        {
            yield return _data[i];
        }
    }

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

    public void Add(T item)
    {
        if (_size == _data.Length)
        {
            Grow();
        }
        _data[_size] = item;
        _size++;
        OnElementAdded(item);
    }

    public void Clear()
    {
        _size = 0;
    }

    public bool Contains(T item)
    {
        return _data.Contains(item);
    }

    public void CopyTo(T[]? array, int arrayIndex)
    {
        if (array is null)
        {
            throw new ArgumentNullException(nameof(array));
        }

        if (arrayIndex < 0)
        {
            throw new ArgumentOutOfRangeException(nameof(arrayIndex));
        }
        Array.Copy(_data, 0, array, arrayIndex, _size);
    }

    public bool Remove(T item)
    {
        var index = IndexOf(item);
        if (index < 0) return false;

        RemoveAt(index);
        return true;

    }

    public int Count => _size;
    
    public int Capacity
    {
        get => _data.Length;
        set
        {
            if (value < _size)
            {
                throw new ArgumentOutOfRangeException(nameof(value));
            }

            if (value != _data.Length)
            {
                if (value > 0)
                {
                    var newData = new T[value];
                    if (_size > 0)
                    {
                        Array.Copy(_data, newData, _size);
                    }
                    _data = newData;
                }
                else
                {
                    _data = Array.Empty<T>();
                }
            }
        }
    }
    public bool IsReadOnly => false;
    public int IndexOf(T item) => Array.IndexOf(_data, item, 0);

    public void Insert(int index, T item)
    {
        _size++;
        var dataIndex = GetDataIndex(index);
        Array.Copy(_data, dataIndex, _data, dataIndex + 1, _size - dataIndex - 1);
        _data[dataIndex] = item;
    }

    public void RemoveAt(int index)
    {
        var dataIndex = GetDataIndex(index);
        _size--;
        OnElementRemoved(_data[dataIndex]);
        if (dataIndex < _size)
        {
            Array.Copy(_data, dataIndex + 1, _data, dataIndex, _size - dataIndex);
        }
        _data[_size] = default!;
    }

    public T this[int index]
    {
        get => _data[GetDataIndex(index)];

        set => _data[GetDataIndex(index)] = value;
    }

    private int GetDataIndex(int index)
    {
        var dataIndex = index % _size;
        if (dataIndex < 0)
        {
            dataIndex = _size + dataIndex;
        }

        return dataIndex;
    }

    private void Grow()
    {
        var newCapacity = _data.Length == 0 ? DefaultCapacity : 2 * _data.Length;
        var newData =  new T[newCapacity];
        for (var i = 0; i < _data.Length; i++)
        {
            newData[i] = _data[i];
        }
        _data = newData;
    }

    protected virtual void OnElementAdded(T value)
    {
        ElementAdded?.Invoke(this, new CustomListEventArgs<T>(value));
    }
    
    protected virtual void OnElementRemoved(T value)
    {
        ElementRemoved?.Invoke(this, new CustomListEventArgs<T>(value));
    }
}