﻿using CollectionLibrary;

CustomList<int> customList = new();
customList.ElementAdded += (_, eventArgs) => Console.WriteLine($"Element was added: {eventArgs.Value}");
customList.ElementRemoved += (_, eventArgs) => Console.WriteLine($"Element was removed: {eventArgs.Value}");
customList.Add(1);
customList.Add(2);
customList.Add(3);
customList.Add(4);
customList.Add(5);
customList.Remove(3);
customList.Remove(2);
customList.Add(6);
customList.Add(7);

Console.WriteLine("Custom list content:");
foreach (var element in customList)
{
    Console.Write($"{element} ");
}
Console.WriteLine();
Console.WriteLine($"Length: {customList.Count}");
Console.WriteLine($"List contains 5: {customList.Contains(5)}");
customList[-1] = -1;
customList[-2] = -1;
customList[5] = -1;
customList[6] = -1;
Console.WriteLine("Custom list content after indexers settings:");
foreach (var element in customList)
{
    Console.Write($"{element} ");
}
Console.WriteLine();

var array = new int[5];
customList.CopyTo(array, 0);
Console.WriteLine("Copied array:");
foreach (var element in customList)
{
    Console.Write($"{element} ");
}
Console.WriteLine();