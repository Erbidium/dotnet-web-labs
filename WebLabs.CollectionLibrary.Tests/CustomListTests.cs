using CollectionLibrary;

namespace WebLabs.CollectionLibrary.Tests;

public class CustomListTests
{
    private readonly CustomList<int> _customList;

    public CustomListTests() =>
        _customList = new CustomList<int>();

    [Fact]
    public void ConstructorWithEnumerableCollection_InitializeCustomListWithNull_ThrownArgumentNullException() =>
        Assert.Throws<ArgumentNullException>(() => new CustomList<int>(null));

    [Fact]
    public void ConstructorWithEnumerableCollection_InitializeCustomListWithEnumerable_InitializedCorrectly()
    {
        var expected = new List<int> { 1, 2, 3, 4, 5 };
        
        var customList = new CustomList<int>(expected);

        Assert.Equal(expected, customList);
    }

    [Fact]
    public void ConstructorWithEnumerableCollection_InitializeCustomListWithStack_InitializedCorrectly()
    {
        var expected = new Stack<int>();
        expected.Push(1);
        expected.Push(2);
        expected.Push(3);
        
        var customList = new CustomList<int>(expected);

        Assert.Equal(expected, customList);
    }
    
    [Fact]
    public void ConstructorWithEnumerableCollection_InitializeCustomListWithEmptyArray_InitializedCorrectly()
    {
        var expected = Array.Empty<int>();

        var customList = new CustomList<int>(expected);

        Assert.Equal(expected, customList);
    }

    [Theory]
    [InlineData(1)]
    [InlineData(1, 2)]
    [InlineData(1, 2, 3)]
    [InlineData(1, 2, 3, 4, 5, 6, 7, 8)]
    public void Add_AddElementToList_ElementAddedCorrectly(params int[] elementsToAdd)
    {
        foreach (var element in elementsToAdd)
        {
            _customList.Add(element);
        }
        
        Assert.Equal(elementsToAdd, _customList);
    }

    [Fact]
    public void Add_AddElementOneToList_ElementAddedEventRaisedWithValueOne()
    {
        const int expected = 1;

        var raisedEvent = Assert.Raises<CustomListEventArgs<int>>(
            a => _customList.ElementAdded += a,
            a => _customList.ElementAdded -= a,
            () => _customList.Add(1)
        );
        Assert.Equal(expected, raisedEvent.Arguments.Value);
    }
    
    [Fact]
    public void Clear_CallClear_CustomListContainsZeroElements()
    {
        _customList.Add(1);
        _customList.Add(2);

        _customList.Clear();
        
        Assert.Equal(_customList, Array.Empty<int>());
    }

    [Theory]
    [InlineData(1, new []{1, 2, 3})]
    [InlineData(2, new []{1, 2, 3})]
    [InlineData(3, new []{1, 2, 3})]
    public void Contains_CallContainsExistingElement_ReturnsTrue(int elementToCheck, int[] listElements)
    {
        var customList = new CustomList<int>(listElements);

        var actual = customList.Contains(elementToCheck);

        Assert.True(actual);
    }
    
    [Fact]
    public void Contains_CallContainsWithNotExistingArgument_DoesNotContain() => 
        Assert.DoesNotContain(0, _customList);

    [Fact]
    public void CopyTo_NullArray_ThrowsArgumentNullException() =>
        Assert.Throws<ArgumentNullException>(() => _customList.CopyTo(null, 0));

    [Fact]
    public void CopyTo_NegativeIndex_ThrowsArgumentOutOfRangeException() => 
        Assert.Throws<ArgumentOutOfRangeException>(() => _customList.CopyTo(new int[3], -1));

    [Fact]
    public void CopyTo_ValidArgumentsGiven_CorrectlyCopiedToArray()
    {
        _customList.Add(1);
        _customList.Add(2);
        _customList.Add(3);
        var expected = new []{1, 2, 3};
        var actual = new int[3];

        _customList.CopyTo(actual, 0);
        
        Assert.Equal(expected, actual);
    }
    
    [Fact]
    public void CopyTo_CustomListLengthIsBiggerThanArrayToCopyLengthFromArrayIndexToEnd_ThrowsArgumentException()
    {
        _customList.Add(1);
        _customList.Add(2);
        _customList.Add(3);
        
        Assert.Throws<ArgumentException>(() => _customList.CopyTo(new int[2], 0));
    }
    
    [Fact]
    public void CopyTo_ArrayIndexIsBiggerThanArrayToCopyLength_ThrowsArgumentException() =>
        Assert.Throws<ArgumentException>(() => _customList.CopyTo(new int[3], 5));

    [Fact]
    public void Remove_NotExistingElement_ReturnsFalse() =>
        Assert.False(_customList.Remove(1));

    [Theory]
    [InlineData(1, new []{1, 2, 3}, new []{2, 3})]
    [InlineData(2, new []{1, 2, 3}, new []{1, 3})]
    [InlineData(3, new []{1, 2, 3}, new []{1, 2})]
    public void Remove_ExistingElement_ReturnsTrue(int elementToRemove, int[] listData, int[] expected)
    {
        foreach (var element in listData)
        {
            _customList.Add(element);
        }

        Assert.True(_customList.Remove(elementToRemove));
        Assert.Equal(expected, _customList);
    }
    
    [Fact]
    public void Remove_RemoveElementOneFromList_ElementRemovedEventRaisedWithValueOne()
    {
        _customList.Add(1);
        const int expected = 1;

        var raisedEvent = Assert.Raises<CustomListEventArgs<int>>(
            a => _customList.ElementRemoved += a,
            a => _customList.ElementRemoved -= a,
            () => _customList.Remove(1)
        );
        Assert.Equal(expected, raisedEvent.Arguments.Value);
    }

    [Theory]
    [InlineData(1, new []{1})]
    [InlineData(2, new []{1, 2})]
    [InlineData(3, new []{1, 2, 3})]
    public void Count_AddedElements_CountIsCorrect(int expectedCount, int[] listData)
    {
        foreach (var element in listData)
        {
            _customList.Add(element);
        }

        Assert.Equal(expectedCount, _customList.Count);
    }
    
    [Fact]
    public void Capacity_InitializeCustomListWithPositiveCapacity_CapacityPropertyReturnsCorrectResult()
    {
        const int expectedCapacity = 5;
        var customList = new CustomList<int>(expectedCapacity);

        var actualCapacity = customList.Capacity;
        
        Assert.Equal(expectedCapacity, actualCapacity);
    }
    
    [Fact]
    public void Capacity_InitializeCustomListWithNegativeCapacity_ThrownArgumentOutOfRangeException() =>
        Assert.Throws<ArgumentOutOfRangeException>(() => new CustomList<int>(-1));

    [Fact]
    public void Capacity_SetCapacitySmallerThanCustomListSize_throwsArgumentOutOfRangeException()
    {
        _customList.Add(1);
        _customList.Add(2);
        _customList.Add(3);

        Assert.Throws<ArgumentOutOfRangeException>(() => _customList.Capacity = 1);
    }
    
    [Fact]
    public void Capacity_SetCapacitySmallerThanCurrentCapacityForEmptyCustomList_CapacityModifiedSuccessfully()
    {
        var customList = new CustomList<int>(10);

        customList.Capacity = 5;

        Assert.Equal(5, customList.Capacity);
    }
    
    [Fact]
    public void Capacity_SetCapacitySmallerThanCurrentCapacityForNotEmptyCustomList_CapacityModifiedSuccessfully()
    {
        var customList = new CustomList<int>(10)
        {
            1,
            2,
            3
        };

        customList.Capacity = 5;

        Assert.Equal(5, customList.Capacity);
        Assert.Equal(new []{1, 2, 3}, customList);
    }
    
    [Fact]
    public void Capacity_SetZeroCapacityForEmptyCustomList_CapacityModifiedSuccessfully()
    {
        var customList = new CustomList<int>(5);

        customList.Capacity = 0;

        Assert.Equal(0, customList.Capacity);
    }

    [Theory]
    [InlineData(0, 1, new []{1, 2, 3})]
    [InlineData(1, 2, new []{1, 2, 3})]
    [InlineData(2, 3, new []{1, 2, 3})]
    public void IndexOf_SearchExistingElement_ReturnsCorrectIndex(int expectedIndex, int elementToFind, int[] listData)
    {
        foreach (var element in listData)
        {
            _customList.Add(element);
        }

        var actualIndex = _customList.IndexOf(elementToFind);
        
        Assert.Equal(expectedIndex, actualIndex);
    }
    
    [Fact]
    public void IndexOf_SearchNotExistingElement_ReturnsNegativeOne()
    {
        _customList.Add(1);
        _customList.Add(2);
        _customList.Add(3);
        const int expectedIndexValue = -1;
        const int notExistingElementValue = 5;

        var actualIndexValue = _customList.IndexOf(notExistingElementValue);
        
        Assert.Equal(expectedIndexValue, actualIndexValue);
    }

    [Fact]
    public void Insert_ToEndOfCustomListWithNegativeIndex_InsertedCorrectly()
    {
        _customList.Add(1);
        _customList.Add(2);
        _customList.Add(3);
        var expected = new [] { 1, 2, 3, 4 };
        const int valueToInsert = 4;
        
        _customList.Insert(-1, valueToInsert);

        Assert.Equal(expected, _customList);
    }
    
    [Fact]
    public void Insert_ToEndOfCustomListWithIndexEqualToLength_InsertedCorrectly()
    {
        _customList.Add(1);
        _customList.Add(2);
        _customList.Add(3);
        var expected = new [] { 1, 2, 3, 4};
        const int valueToInsert = 4;
        
        _customList.Insert(_customList.Count, valueToInsert);

        Assert.Equal(expected, _customList);
    }
    
    [Fact]
    public void Insert_ToStartOfCustomListWithIndexMoreThanLength_InsertedCorrectly()
    {
        _customList.Add(1);
        _customList.Add(2);
        _customList.Add(3);
        var expected = new [] { 4, 1, 2, 3};
        const int valueToInsert = 4;
        
        _customList.Insert(_customList.Count + 1, valueToInsert);

        Assert.Equal(expected, _customList);
    }
    
    [Fact]
    public void RemoveAt_WithIndexNegativeOne_RemovedLastElement()
    {
        _customList.Add(1);
        _customList.Add(2);
        _customList.Add(3);
        var expected = new []{1, 2};

        _customList.RemoveAt(-1);

        Assert.Equal(expected, _customList);
    }
    
    [Fact]
    public void RemoveAt_WithIndexEqualToCustomListLength_RemovedFirstElement()
    {
        _customList.Add(1);
        _customList.Add(2);
        _customList.Add(3);
        var expected = new []{2, 3};

        _customList.RemoveAt(3);

        Assert.Equal(expected, _customList);
    }
    
    [Fact]
    public void RemoveAt_RemoveFirstElementFromList_ElementRemovedEventRaisedWithValueOfFirstElement()
    {
        _customList.Add(1);
        const int expected = 1;

        var raisedEvent = Assert.Raises<CustomListEventArgs<int>>(
            a => _customList.ElementRemoved += a,
            a => _customList.ElementRemoved -= a,
            () => _customList.RemoveAt(0)
        );
        Assert.Equal(expected, raisedEvent.Arguments.Value);
    }
    
    [Fact]
    public void Indexer_WithIndexNegativeOne_ReturnsLastElement()
    {
        _customList.Add(1);
        _customList.Add(2);
        _customList.Add(3);
        const int expected = 3;

        var actual = _customList[-1];

        Assert.Equal(expected, actual);
    }
    
    [Fact]
    public void Indexer_WithIndexEqualToLength_ReturnsFirstElement()
    {
        _customList.Add(1);
        _customList.Add(2);
        _customList.Add(3);
        const int expected = 1;

        var actual = _customList[3];

        Assert.Equal(expected, actual);
    }
    
    [Fact]
    public void Indexer_SetValueWithIndexNegativeOne_LastElementChanged()
    {
        _customList.Add(1);
        _customList.Add(2);
        _customList.Add(3);
        const int expected = 4;
        const int negativeIndex = -1;

        _customList[negativeIndex] = expected;
        var actual = _customList[negativeIndex];

        Assert.Equal(expected, actual);
    }
    
    [Fact]
    public void Indexer_SetValueWithIndexEqualToLength_FirstElementChanged()
    {
        _customList.Add(1);
        _customList.Add(2);
        _customList.Add(3);
        const int expected = 4;
        const int index = 3;

        _customList[index] = expected;
        var actual = _customList[index];

        Assert.Equal(expected, actual);
    }
}